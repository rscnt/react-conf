import ExtractTextPlugin from 'extract-text-webpack-plugin';

export const babelLoader = {
  test: /\.js$/,
  loader: 'babel',
  exclude: /node_modules/,
  query: {stage: 0, plugins: ['./tools/babelRelayPlugin']}
};


export const cssLoader = {
  test: /\.css$/,
  loader: ExtractTextPlugin.extract('style-loader', 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader')
};

export const auth0Loader = {
  test: /node_modules\/auth0-js\/.*\.js$/,
  loaders: ['transform/cacheable?brfs', 'transform/cacheable?packageify']
};

export const ejsLoader = {test: /\.ejs$/, loader: 'ejs-compiled'};

export const jsonLoader = {test: /\.json$/, loader: 'json'};
