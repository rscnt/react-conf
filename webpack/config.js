// PostCSS plugins
import autoprefixer from 'autoprefixer';
import precss from 'precss';
import cssnext from 'cssnext';
import nested from 'postcss-nested';
import rucksack from 'rucksack-css';
import sprites from 'postcss-sprites';
import assets from 'postcss-assets';
import lost from 'lost';

// Webpack Plugins + Loaders
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import { babelLoader, cssLoader } from './loaders';
import {HotModuleReplacementPlugin, DefinePlugin} from 'webpack';

// Libraries
import { resolve } from 'path';

const assetOptions = {
  cachebuster: true
};

const postcssPlugins = [cssnext, autoprefixer, precss, nested, sprites, assets(assetOptions), lost, rucksack()];
const plugins = [
  new ExtractTextPlugin('style.css', { allChunks: true }),
  new DefinePlugin({
    'process.env': JSON.stringify(process.env)
  })
];

let entry = resolve(__dirname, '../js/app.js');

if (process.env.NODE_ENV !== 'production') {
  entry = {
    app: [
      `webpack-dev-server/client?http://localhost:${process.env.PORT}`,
      'webpack/hot/dev-server',
      entry
    ]
  };
  plugins.push(new HotModuleReplacementPlugin());
}

export default {
  entry,
  output: {
    filename: 'app.js',
    path: resolve('./dist')
  },
  resolve: {
    alias: {
      styles: resolve('./styles/main.css')
    }
  },
  module: {
    loaders: [ babelLoader ]
  },
  postcss: () => postcssPlugins,
  plugins
};
