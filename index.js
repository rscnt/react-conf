import './config';
import webpack from 'webpack';
import express from 'express';
import config from './webpack/config';
import WebpackDevServer from 'webpack-dev-server';

let app = express();

if (process.env.NODE_ENV !== 'production') {
  const compiler = webpack(config);
  app = new WebpackDevServer(compiler, {
    contentBase: '/public/',
    publicPath: '/',
    hot: true,
    stats: { colors: true },
    historyApiFallback: true
  });
  app.use('/', express.static('public'));
} else {
  app.use('/', express.static('dist'));
}

app.listen(process.env.PORT);
