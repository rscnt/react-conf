import gulp from 'gulp';
import mocha from 'gulp-mocha';
import eslint from 'gulp-eslint';

gulp.task('mocha', () => {
  return gulp.src(['spec/**/*.js', '!spec/util/*.js'], {read: false})
    .pipe(mocha({reporter: 'spec'}));
});

gulp.task('lint', () => {
  return gulp.src(['lib/**/*.js', 'spec/**/*.js', '!lib/models/seeders/*.js'])
    .pipe(eslint())
    .pipe(eslint.formatEach())
    .pipe(eslint.failAfterError());
});

gulp.task('test', gulp.series('lint', 'mocha'));
