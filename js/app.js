import createHashHistory from 'history/lib/createHashHistory';
import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, IndexRoute} from 'react-router';

import App from './components/app';
import Home from './pages/home';

const history = createHashHistory({queryKey: false});

ReactDOM.render(
  <Router history={history}>
    <Route
      path='/' component={App}>
      <IndexRoute component={Home}/>
    </Route>
  </Router>,
  document.getElementById('root')
);
