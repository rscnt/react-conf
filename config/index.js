import dotenv from 'dotenv';

const env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const envPath = `.${env}`;
let loaded = false;

if (env === 'production') {
  // Heroku Don't load no config vars!
  loaded = true;
}

if (loaded === false) {
  dotenv.config({path: __dirname + '/.env.default'});
  dotenv.config({path: __dirname + `/.env${envPath}`});
  loaded = true;
}
