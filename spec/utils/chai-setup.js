import chai from 'chai';
import asPromised from 'chai-as-promised';
import things from 'chai-things';

chai.use(things);
chai.use(asPromised);

export const expect = chai.expect;
export const should = chai.should;
export const assert = chai.assert;
