import {describe, it, before} from 'mocha';
import {expect} from './utils/chai-setup';
import Sample from '../sample';

let sample;

describe('Sample code test case', () => {
  before('Get an instance of Sample for testing', () => {
    sample = new Sample;
  });

  it('Should eventually equal 3', () => {
    return expect(sample.resolvePromise()).to.eventually.equal(3);
  });

  it('Should not equal 6, but want to show a failed test using promises', () => {
    return expect(sample.resolvePromise()).to.eventually.equal(6);
  });
});
